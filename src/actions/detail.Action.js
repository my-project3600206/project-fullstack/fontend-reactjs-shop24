import { ADD_TO_CART, CHANGE_INPUT_VOUCHER, CHANGE_NUMBER_PAGE_CART, EDIT_ADD_QUANITY, EDIT_MINUS_QUANITY, FETCH_VOUCHER_ERROR, FETCH_VOUCHER_PENDING, FETCH_VOUCHER_SUCCESS, LOAD_DATA_LOCAL_STORAGE, REMOVE_CART, UPDATE_CART } from "../constants/detail.Content"

export const addToCart =(quanityProduct, productById)=>{
    return {
        type: ADD_TO_CART,
        payload: {quanityProduct, productById}
    }
}
export const loadDataLocalStorage = ()=>{
    return {
        type: LOAD_DATA_LOCAL_STORAGE
    }
}
export const editAddQuanity = (idProduct)=>{
    return {
        type: EDIT_ADD_QUANITY,
        payload: {idProduct}
    }
}
export const editMinusQuanity = (idProduct)=>{
    return {
        type: EDIT_MINUS_QUANITY,
        payload: {idProduct}
    }
}
export const changePageCart = (currentPage)=>{
    return {
        type: CHANGE_NUMBER_PAGE_CART,
        payload:{currentPage}
    }
}
export const removeCart = (idProduct)=>{
    return {
        type: REMOVE_CART,
        payload: {idProduct}
    }
}
export const updateCart = ()=>{
    return {
        type: UPDATE_CART
    }
}
export const changeInputVoucher = (voucherCode)=>{
    return{
        type: CHANGE_INPUT_VOUCHER,
        payload: {voucherCode}
    }
}
export const fetchApiCheckVoucherCode = (voucherCode)=>{
    return async(dispatch)=>{
        try{
            await dispatch ({
                type: FETCH_VOUCHER_PENDING
            })
            const data = await fetch (`http://localhost:8000/voucher/code/${voucherCode}`)
            const resVoucher = await data.json()
            console.log("voucher", resVoucher)
            return dispatch ({
                type: FETCH_VOUCHER_SUCCESS,
                payload: {discount: resVoucher.voucher.discount,
                    idVoucher: resVoucher.voucher._id
                }
            })
        }
       catch (err){
        return dispatch ({
            type: FETCH_VOUCHER_ERROR,
            err: err.message
        })
       }
        
    }
}