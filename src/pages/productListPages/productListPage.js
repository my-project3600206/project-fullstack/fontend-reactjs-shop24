import AllProduct from "../../components/allProducts/allProduct"
import BreadCrumb from "../../components/breadCrumb/breadCrumb"

function ProductListPage() {
    const breadCrumbList =
        [
            {
                url: "/",
                name: "Home"
            },
            {
                url: "/products",
                name: "All Product"
            }
        ]

    return (
        <>
            <div>
                <BreadCrumb breadCrumbList={breadCrumbList} />
                <div className="mt-3">
                    <AllProduct />
                </div>


            </div>
        </>
    )
}
export default ProductListPage