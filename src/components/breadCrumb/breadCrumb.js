import { Breadcrumb, BreadcrumbItem } from "reactstrap";
import "../../components/breadCrumb/breadCrumb.css"
import { useNavigate } from "react-router-dom";

const style = {backgroundColor:"#F5F5F5", height:"45px", paddingTop:"15px"}
function BreadCrumb({ breadCrumbList }) {
    const navigate = useNavigate();
    const handBreadCrumbClick = (url)=>{
        navigate(url)
    }
    return (
        <div className="breadCrumb" style={style}>
            <div className="container" >
                <Breadcrumb>
                    {breadCrumbList.map((item, index) => {
                        if (index !== breadCrumbList.leng - 1) {
                            return (
                                <BreadcrumbItem key={index}>
                                    <span onClick={()=>handBreadCrumbClick(item.url)}  >{item.name}</span>
                                </BreadcrumbItem>
                            )
                        } else {
                            return (
                                <BreadcrumbItem key={index} active >{item.name}</BreadcrumbItem>
                            )
                        }
                    })}
                </Breadcrumb>
            </div>

        </div>
    )
}
export default BreadCrumb