import { CHANGE_CHECKED_FILTER, CHANGE_INPUT_SEARCH, CHANGE_PRICEMAX_FILTER, CHANGE_PRICEMIN_FILTER, RESET_FILTER } from "../constants/filter.Content"


export const changeCheckedFilter = (statusChecked, valueChecked, typeChecked)=>{
    return {
        type: CHANGE_CHECKED_FILTER,
        payload: {
            statusChecked,
            valueChecked,
            typeChecked
        }
    }
}
export const changeFilterPriceMin = (priceMin)=>{
    return {
        type: CHANGE_PRICEMIN_FILTER,
        payload: {
            priceMin
        }
    }
}
export const changeFilterPriceMax = (priceMax)=>{
    return {
        type: CHANGE_PRICEMAX_FILTER,
        payload: {
            priceMax
        }
    }
}
export const changeInPutSearch = (valueInput)=>{
    return {
        type: CHANGE_INPUT_SEARCH,
        payload: {
            valueInput
        }
    }
}
export const resetFilter = ()=>{
    return {
        type: RESET_FILTER
    }
}