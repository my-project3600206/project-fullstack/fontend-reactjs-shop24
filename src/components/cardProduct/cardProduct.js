import { useNavigate } from "react-router-dom"
import "../contentComponents/content.Component.css"


function CardProduct({ productId, name, imageUrl, description, buyPrice, promotionPrice }) {
    const navigate = useNavigate();

    const handClickCardProduct = ()=>{
        navigate(`/productDetail/${productId}`)
    }

    return (
        <>
            <div className="card-wraper">
                <div className="card" >
                        <div className="thumbnail">
                            <img src={imageUrl} className="card-img-top" alt="" onClick={handClickCardProduct}/>
                        </div>
                    <div className="card-body">
                        <h6 className="card-title">{name}</h6>
                        <div className="card-text">
                            <p className="price" >{buyPrice} $</p>
                            <p className="pro-price">{promotionPrice} $</p>
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
}
export default CardProduct