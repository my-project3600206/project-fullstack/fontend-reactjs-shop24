import { applyMiddleware, combineReducers, legacy_createStore } from "redux";
import thunk from "redux-thunk";
import productReducer from "./product.Reducer";
import filterReducer from "./filter.Reducer";
import detailReducer from "./detail.Reducer";
const rootReducer = combineReducers({
    productReducer, filterReducer, detailReducer
})
const store = legacy_createStore(rootReducer, applyMiddleware(thunk))
export default store