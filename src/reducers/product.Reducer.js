
import { FETCH_BRAND_COLOR_SUCCESS, FETCH_PRODUCT_BY_ID_PENDING, FETCH_PRODUCT_BY_ID_SUCCESS, FETCH_PRODUCT_PENDING, FETCH_PRODUCT_SUCCESS } from "../constants/product.Content";

const initialData = {
    fetchProductPending: false,
    productList: [],
    productBrandList: [],
    productColorList:[],
    itemPerPage: 6,
    currentPage: 1,
    numberPage: 1,
    fetProductByIdPending: false,
    productById: {}


}
const productReducer = ((state = initialData, action) => {
    switch (action.type) {
        case FETCH_PRODUCT_PENDING:
            state.fetchProductPending = true
            break;
        case FETCH_PRODUCT_SUCCESS:
            state.fetchProductPending = false
            state.productList = action.payload.resProductList
            state.numberPage = action.payload.numberPage
            state.currentPage = action.payload.currentpage
            break;
        case FETCH_BRAND_COLOR_SUCCESS:
            state.productBrandList = action.payload.resData.data.brand
            state.productColorList = action.payload.resData.data.color
            
            break;
        case FETCH_PRODUCT_BY_ID_PENDING:
            state.fetProductByIdPending = true
            break;
        case FETCH_PRODUCT_BY_ID_SUCCESS:
            state.fetProductByIdPending = false
            state.productById = action.payload.resData.product
            
            break;

        default:
            break;
    }

    return { ...state }
})
export default productReducer