import { Button, TextField } from "@mui/material"

const style={width: "350px", height: "480px", backgroundColor: "#ffff",position:"relative", margin:"160px auto", overflow:"hidden", border:"1px solid #D3D3D3", borderRadius: "15px", boxShadow:"5px 10px #D3D3D3"}
function SignUpForm(){

    return(
        <>
        <div className="sign-wraper" style={style}  >
                <div className="container">
                    <div className="sign-form">
                        <div>
                            <p className="title">SIGN UP</p>
                        </div>
                        
                        <div>
                            <TextField
                                required
                                id="standard-required"
                                label="User Name"
                                variant="standard"
                                fullWidth
                            />
                        </div>
                        <div>
                            <TextField
                                required
                                id="standard-required"
                                label="Email"
                                variant="standard"
                                fullWidth
                            />
                        </div>
                        <div>
                            <TextField
                                required
                                id="standard-required"
                                label="Phone Number"
                                variant="standard"
                                fullWidth
                            />
                        </div>
                        <div>
                            <TextField
                               required
                               id="standard-password-input"
                               label="Password"
                               type="password"
                               autoComplete="current-password"
                               variant="standard"
                               fullWidth
                               
                            />
                        </div>
                        <div style={{marginTop: "30px"}}>
                           <Button className="btn-login" fullWidth >LOGIN</Button>
                        </div>
                       
                    </div>
                </div>
            </div>
        </>
    )
}
export default SignUpForm