import { Link } from "react-router-dom"
import "../../contentComponents/content.Component.css"
import { useSelector } from "react-redux"

function ViewAllProduct() {
    const respPoductInfo = useSelector((reduxData) => {
        return (reduxData.productReducer)
    })
    return (
        <>
            <div className="view-All">
                <Link to={"/products"}>
                    {respPoductInfo.productList.length > 0 ? (<button className="btn-view">ViewAllProduct</button>) :null}
                </Link>
            </div>
        </>
    )
}
export default ViewAllProduct