import { useDispatch, useSelector } from "react-redux"
import { changeInPutSearch } from "../../actions/filter.Action";
import { useNavigate } from "react-router-dom";
import { fetApiGetProduct } from "../../actions/product.Action";

function Search() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const conditionFilter = useSelector((reduxdata)=>{
        return (reduxdata.filterReducer)
    })
    const productInfor = useSelector((reduxdata)=>{
        return (reduxdata.productReducer)
    })

    const handChangeInPutSearch = (e)=>{
        console.log("inp", e.target.value)
        dispatch(changeInPutSearch(e.target.value))
    }
    const handClickSearchBtn = ()=>{
        if(conditionFilter.name){
            navigate(`/products?keyword=${conditionFilter.name}`)
        }else{
            navigate(`/products`)
        }
        dispatch(fetApiGetProduct(1, productInfor.itemPerPage, conditionFilter))
    }
    const styleSearch = {display: "flex", width:"500px"}
    return (
        <>
        <div className="form-group" style={styleSearch}>
            <input type="text" placeholder="Search..." className="form-control" value={conditionFilter.name} onChange={handChangeInPutSearch}/>
            <button className="btn btn-primary" onClick={handClickSearchBtn} >Search</button>
        </div>
        </>
    )
}
export default Search