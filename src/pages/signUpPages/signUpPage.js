import BreadCrumb from "../../components/breadCrumb/breadCrumb"
import SignUpForm from "../../components/contentComponents/signUpForm/signUpForm"

function SignUpPage (){
    const breadCrumbList = [
        {
            url: "/",
            name: "Home"
        },
        {
            url: "/signUp",
            name: "SignUp"
        }
    ]
    return (
        <>
        <div>
            <BreadCrumb breadCrumbList = {breadCrumbList}/>
        </div>
        <div>
            <SignUpForm/>
        </div>
        
        </>
    )
}
export default SignUpPage