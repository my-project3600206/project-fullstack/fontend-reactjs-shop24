import { Link, useNavigate } from "react-router-dom"
import Search from "../search/search"
import PhoneInTalkIcon from '@mui/icons-material/PhoneInTalk';
import "../../components/headers/headerComponent.css";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useDispatch, useSelector } from "react-redux";
import { resetFilter } from "../../actions/filter.Action";
import { Badge, IconButton, MenuItem } from "@mui/material";
function HeaderComponent() {
    const dispatch = useDispatch()
    const navigation = useNavigate();
    const detailReducer = useSelector((reduxData)=>{
        return (reduxData.detailReducer)
    })
    const handClickLogo = ()=>{
        window.scrollTo(0,0);
        dispatch(resetFilter())

    }
    const handLoginClick = ()=>{
        navigation("/login")
    }
    return (
        <>
            <nav className="navbar navbar-expand-md navbar-light bg-light fixed-top">
                <div className="container">
                    <div className="header-brand">
                        <Link to={"/"}>
                            <img src="https://theme.hstatic.net/1000152881/1000926405/14/logo.png?v=642" alt="logo" height={50} onClick={handClickLogo} />
                        </Link>
                    </div>
                    <div className="header-search">
                        <Search />
                    </div>
                    <div className="header-right">
                        <div className="info-right">
                            <div>
                                <PhoneInTalkIcon style={{ fontSize: "25px" }} />
                            </div>
                            <div >
                                <div>Gọi mua hàng</div>
                                <div><b>01234567</b></div>
                            </div>
                        </div>
                        
                        <div className="info-right">
                            <div>
                                <AccountCircleIcon style={{ fontSize: "25px" }} onClick={handLoginClick}/>
                            </div>
                            <div >
                                <div onClick={handLoginClick}>Tài khoản</div>
                                <div onClick={handLoginClick}><b>Đăng nhập</b></div>
                            </div>
                        </div>
                        <div className="info-right">
                           <Link to={"/shopCart"}>
                            <div className="header_shop">
                                    <MenuItem style={{backgroundColor:"#fff000"}}>
                                        <IconButton size="large" aria-label="show 4 new mails" color="inherit" >
                                            <Badge badgeContent={detailReducer.productSelected.length } color="error">
                                                <ShoppingCartIcon />
                                            </Badge>
                                            
                                        </IconButton>
                                        <p className="shopCart">Giỏ hàng</p>
                                    </MenuItem>
                                </div>
                           </Link>
                            
                        </div>
                    </div>
                </div>
            </nav>
        </>
    )
}
export default HeaderComponent