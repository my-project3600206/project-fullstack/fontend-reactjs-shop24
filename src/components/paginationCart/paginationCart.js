import { Container, Grid, Pagination, Stack } from "@mui/material"
import { useDispatch } from "react-redux"
import { changePageCart } from "../../actions/detail.Action";


function PaginationCart({numberPage}){
    const dispatch = useDispatch();
    const handChangePageCart = (e, value)=>{
        dispatch(changePageCart(value))
    }
    return (
        <>
        <Container>
            <Grid container mt={3} >
                <Grid item>
                    <Stack spacing={2}>
                        <Pagination onChange={handChangePageCart} count={numberPage} color="secondary" />
                    </Stack>
                </Grid>

            </Grid>
        </Container>
       
        </>
    )
}
export default PaginationCart