import { Button, TextField } from "@mui/material"
import "../loginForm/loginForm.css"
import PersonIcon from '@mui/icons-material/Person';
import GoogleIcon from '@mui/icons-material/Google';
import { Link, useNavigate } from "react-router-dom";

const style={width: "350px", height: "420px", backgroundColor: "#ffff",position:"relative", margin:"160px auto", overflow:"hidden", border:"1px solid #D3D3D3", borderRadius: "15px", boxShadow:"5px 10px #D3D3D3"}
function LoginForm (){
    const navigate = useNavigate()
    const handSignUp = ()=>{
        navigate("/signUp")
    }
    
    return (
        <>
            <div className="login-wraper" style={style}  >
                <div className="container">
                    <div className="login-form">
                        <div>
                            <p className="title">LOGIN</p>
                        </div>
                        
                        <div>
                            <TextField
                                required
                                id="standard-required"
                                label="User Name"
                                variant="standard"
                                fullWidth
                            />
                        </div>
                        <div>
                            <TextField
                               required
                               id="standard-password-input"
                               label="Password"
                               type="password"
                               autoComplete="current-password"
                               variant="standard"
                               fullWidth
                               
                            />
                        </div>
                        <div style={{marginTop: "30px"}}>
                           <Button className="btn-login" fullWidth >LOGIN</Button>
                        </div>
                        <div className="infor">
                            <p>Don't have an account? <span style={{color:"green", cursor:"pointer"}} onClick={handSignUp}>Sign up here</span></p>
                            <p className="infor">Or login using </p>
                        </div>
                       
                        <button className="btn-gg"><GoogleIcon style={{color: "white"}}/></button>
                        

                    </div>
                </div>
            </div>
        </>
    )
}
export default LoginForm