import { CHANGE_CHECKED_FILTER, CHANGE_INPUT_SEARCH, CHANGE_PRICEMAX_FILTER, CHANGE_PRICEMIN_FILTER, RESET_FILTER } from "../constants/filter.Content";

const initialData = {
    name: "",
    brand: [],
    color:[],
    price: {
        min: "",
        max: ""
    }
}
const filterReducer = ((state = initialData, action) => {
   switch (action.type) {
    case CHANGE_CHECKED_FILTER:
        if(action.payload.statusChecked){
            state[action.payload.typeChecked].push(action.payload.valueChecked)
        }
        else{
            const newFilterCondition = state[action.payload.typeChecked].filter((conditon)=> conditon !== action.payload.valueChecked)
            state[action.payload.typeChecked] = [...newFilterCondition]
        }
        break;
    case CHANGE_PRICEMIN_FILTER:
            state.price.min = action.payload.priceMin
            console.log("priceMin", state.price.min)
        break;
    case CHANGE_PRICEMAX_FILTER:
            state.price.max = action.payload.priceMax
            
        break;
    case CHANGE_INPUT_SEARCH:
            state.name = action.payload.valueInput
            
        break;
    case RESET_FILTER:
            state.name = ""
            state.brand = []
            state.color = []
            state.price.min =""
            state.price.max = ""
        break;
    default:
        break;
   }

    return { ...state }
}) 
export default filterReducer