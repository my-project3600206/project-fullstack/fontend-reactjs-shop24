
import { Col, Row } from "react-bootstrap";
import Slider from "react-slick"
import "../productDetail/detailCarousel.css"

function DetailCarousel ({resPoductInfo, setImage}){
   const handClickImage =(index)=>{
        setImage(index)
   }
    const settings = {
        dots: true,  // Dấu chấm định danh địa chỉ đên cho mỗi item show
        infinite: true, // Vòng lăp vô tận
        speed: 800, //Tốc độ chuyển slide
        slidesToShow: 3, // Số lượng phần tử được nhìn thấy
        slidesToScroll: 1, //Số phần tử được lướt sang
        arrows: true,  //Cho phép hiển thị phím mũi tên chạy
        autoplay: true,
        autoplaySpeed: 5000, //Tự đông chạy cho slide
       
    };
    return (
        <>
        <div className='carousel'>
            <Slider {...settings}>
                {resPoductInfo?.productById?.imageUrl?.map((item, index) => {
                   
                        return (
                            <div key={index}>
                                <Row className='slide-wraper'>
                                    <Col lg='12' md='12' className='slide-image'>
                                        <img alt='slide' src={item} style={{width: "100%"}} onClick={()=>handClickImage(index)}/>
                                    </Col>
                                </Row>
                            </div>
                        )
                    
                })}

            </Slider>
        </div>
        </>
    )
}
export default DetailCarousel