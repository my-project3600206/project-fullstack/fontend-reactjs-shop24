import { useSelector } from "react-redux"
import CarouselHomePage from "./carouselHomePage/carouselHomePage"
import LastProduct from "./lastestProducts/lastProduct"
import ViewAllProduct from "./viewAll/viewAll"


function ContentComponent () {
    const resProduct = useSelector((reduxData)=>{
        return (reduxData.productReducer)
    })
    return (
        <>
            <div>
                <div className="container">
                    <CarouselHomePage />
                </div>
                
                <LastProduct/>
                {resProduct.fetchProductPending ? null : <ViewAllProduct/>}
                
            </div>

        </>
    )
}
export default ContentComponent