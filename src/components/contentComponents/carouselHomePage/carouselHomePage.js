

import { useSelector } from 'react-redux';
import "../content.Component.css";
import Slider from 'react-slick';
import { Col, Row } from 'react-bootstrap';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


function CarouselHomePage() {
    const resProductList = useSelector((reduxData) => {
        return (reduxData.productReducer)
    })
    console.log("res", resProductList.productList)
    const settings = {
        dots: true,  // Dấu chấm định danh địa chỉ đên cho mỗi item show
        infinite: true, // Vòng lăp vô tận
        speed: 800, //Tốc độ chuyển slide
        slidesToShow: 1, // Số lượng phần tử được nhìn thấy
        slidesToScroll: 1, //Số phần tử được lướt sang
        arrows: true,  //Cho phép hiển thị phím mũi tên chạy
        autoplay: true,
        autoplaySpeed: 5000, //Tự đông chạy cho slide
       
    };
    return (
        <div className='carousel'>
            <Slider {...settings}>
                {resProductList.productList.map((product, index) => {
                    if (index < 4) {
                        return (
                            <div key={index}>
                                <Row className='slide-wraper'>
                                    <Col lg='7' md='12' className='slice' >
                                        <div className='slide-info'>
                                            <p className='sub-title'>Headphone</p>
                                            <h4 className='title'>{product.name}</h4>
                                            <p className='content'>
                                                {product.description}
                                            </p>
                                            <button className='btn-shop'>SHOP NOW</button>
                                        </div>
                                    </Col>
                                    <Col lg='5' md='12' className='slide-image'>
                                        <img alt='slide' src={product.imageUrl[0]} />
                                    </Col>
                                </Row>
                            </div>
                        )
                    }
                    return null
                })}

            </Slider>
        </div>
    )
}
export default CarouselHomePage
