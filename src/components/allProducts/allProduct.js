
import { useDispatch, useSelector } from "react-redux"
import CardProduct from "../cardProduct/cardProduct"
import ProductFilter from "../productFilter/productFilter";
import "../allProducts/allProduct.css"
import { useEffect } from "react";
import { fetApiGetProduct, fetApigetAllBrandAndColor } from "../../actions/product.Action";
import PaginationProduct from "../paginations/pagination";


function AllProduct() {
    const dispatch = useDispatch();
    const ProductInfo = useSelector((reduxData) => {
        return (reduxData.productReducer)
    })
    const conditionFilter = useSelector((reduxData) => {
        return (reduxData.filterReducer)
    })
    console.log("condition", conditionFilter.brand)
    useEffect(() => {
        window.scrollTo(0, 0)
        dispatch(fetApiGetProduct(1, ProductInfo.itemPerPage, conditionFilter))
        dispatch(fetApigetAllBrandAndColor())
    }, [ProductInfo.itemPerPage])
    return (
        <>
        
    <div className="all-product-wrapper">
        <div className="container">
            <div className="row">
                <div className="col-3">
                    <ProductFilter />
                </div>
                <div className="col-9">
                    <div style={{ display: ProductInfo.fetchProductPending ? "none" : "block", textAlign: "center" }}>
                        <h4>ALL PRODUCTS</h4>
                    </div>
                    {ProductInfo.productList.length >0 ? 
                    (<div className="card-product" >
                    {ProductInfo.productList.map((product, index) => (

                        <CardProduct
                            productId={product._id}
                            name={product.name}
                            imageUrl={product.imageUrl[0]}
                            description={product.description}
                            buyPrice={product.buyPrice}
                            promotionPrice={product.promotionPrice} />

                    )

                    )}
                    </div>) : (<div style={{textAlign:"center", marginTop:"100px", fontSize: "30px"}}> No Product </div>)}
                    {ProductInfo.productList.length >0 ?
                    (<div className="pagination">
                    <PaginationProduct numberPage={ProductInfo.numberPage} page={ProductInfo.currentPage} />
                 </div>) : null}
                    

                </div>
            </div>
            
        </div>
    </div>
        
        </>
    )
}
export default AllProduct