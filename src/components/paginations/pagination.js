import { Container, Grid, Pagination, Stack } from "@mui/material"
import "../paginations/pagination.css"
import { useDispatch, useSelector } from "react-redux"
import { fetApiGetProduct } from "../../actions/product.Action";

function PaginationProduct({numberPage, page}) {
    window.scrollTo(0, 0);
    const productInfo = useSelector((reduxData)=>{
        return(reduxData.productReducer)
    })
    const conditionFilter = useSelector((reduxData) => {
        return (reduxData.filterReducer)
    })
    const dispatch = useDispatch();
    const handlerPageOnchange = (e, value)=>{
        dispatch(fetApiGetProduct(value, productInfo.itemPerPage, conditionFilter ))
        console.log("value", value)
    }
    return (
        <>
            <Container>
                <Grid container mt={3} justifyContent={"center"}>
                    <Grid item>
                        <Stack spacing={2}>
                            <Pagination onChange={handlerPageOnchange} page={page} count={numberPage} variant="outlined" shape="rounded" color="primary" className="ul MuiPaginationItem-root MuiPaginationItem-ellipsis Mui-selected" />
                        </Stack>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}
export default PaginationProduct