import BreadCrumb from "../../components/breadCrumb/breadCrumb"
import LoginForm from "../../components/contentComponents/loginForm/loginForm"

function LoginPage (){
    const breadCrumbList = [
        {
            url: "/",
            name: "Home"
        },
        {
            url: "/login",
            name: "Login"
        }
    ]
    return (
        <>
        <div>
            <BreadCrumb breadCrumbList = {breadCrumbList}/>
        </div>
        <div>
            <LoginForm/>
        </div>
        
        </>
    )
}
export default LoginPage