
import {  FETCH_BRAND_COLOR_ERROR, FETCH_BRAND_COLOR_PENDING, FETCH_BRAND_COLOR_SUCCESS, FETCH_PRODUCT_BY_ID_ERROR, FETCH_PRODUCT_BY_ID_PENDING, FETCH_PRODUCT_BY_ID_SUCCESS, FETCH_PRODUCT_ERROR, FETCH_PRODUCT_PENDING, FETCH_PRODUCT_SUCCESS } from "../constants/product.Content"


export const fetApiGetProduct = (currentpage, itemPerPage, conditionFilter) => {

    return async (dispatch) => {
        try {
            await dispatch({
                type: FETCH_PRODUCT_PENDING
            })
            const data = await fetch(`http://localhost:8000/products/filters?page=${currentpage}&limit=${itemPerPage}&brand=${conditionFilter.brand}&color=${conditionFilter.color}&min=${conditionFilter.price.min}&max=${conditionFilter.price.max}&keyword=${conditionFilter.name}`)
            const resProductList = await data.json()
            return dispatch({
                type: FETCH_PRODUCT_SUCCESS,
                payload: {
                    resProductList: resProductList.productList,
                    numberPage: resProductList.numberPage,
                    currentpage,
                    itemPerPage
                }
            })
        }
        catch (err) {
            return dispatch({
                type: FETCH_PRODUCT_ERROR,
                err: err.message
            })
        }
    }
}
export const fetApigetAllBrandAndColor = () => {
    return async (dispatch) => {
        try {
            await dispatch({
                type: FETCH_BRAND_COLOR_PENDING
            })
            const data = await fetch(`http://localhost:8000/products/brands/colors`)
            const resData = await data.json()
            return dispatch({
                type: FETCH_BRAND_COLOR_SUCCESS,
                payload: {
                    resData
                }
            })
        }
        catch (err) {
            return dispatch({
                type: FETCH_BRAND_COLOR_ERROR,
                err: err.message
            })
        }
    }
}
export const fetApiGetProductById = (productId)=>{
    return async(dispatch)=>{
        try {
            await dispatch({
                type: FETCH_PRODUCT_BY_ID_PENDING
            })
            const data = await fetch(`http://localhost:8000/products/${productId}`)
            const resData = await data.json()
            return dispatch({
                type: FETCH_PRODUCT_BY_ID_SUCCESS,
                payload: {resData}
            })
        }
        catch(err) {
            return dispatch({
                type: FETCH_PRODUCT_BY_ID_ERROR,
                err: err.message
            })
        }

    }

}
