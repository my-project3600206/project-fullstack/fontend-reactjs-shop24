import BreadCrumb from "../../components/breadCrumb/breadCrumb"
import ShopCart from "../../components/shopCart/shopCart"

function ShopCartPage (){

    const breadCrumbList = [
        {
            url: "/",
            name: "Home"
        },
        {
            url: "/shopCart",
            name: "Cart"
        }
    ]
    return (
        <>
        <div>
            <BreadCrumb breadCrumbList = {breadCrumbList}/>
        </div>
        <div className="container">
            <ShopCart/>
        </div>
        
        
        </>
    )
}
export default ShopCartPage