
import { useDispatch, useSelector } from "react-redux"
import "../../components/productFilter/productFilter.css"
import { changeCheckedFilter, changeFilterPriceMax, changeFilterPriceMin } from "../../actions/filter.Action";
import { fetApiGetProduct } from "../../actions/product.Action";
import { useNavigate } from "react-router-dom";

function ProductFilter() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const resDataFilter = useSelector((reduxData)=>{
        return(reduxData.productReducer)
    })
    const resConditionFilter = useSelector((reduxData)=>{
        return(reduxData.filterReducer)
    })

    const handcheckedFilter = (e)=>{
        dispatch(changeCheckedFilter(e.target.checked, e.target.value, e.target.dataset.type))
        console.log(e.target.value)
        console.log(e.target.checked)
        console.log(e.target.dataset.type)
    }
    const handChangeFilterPriceMin = (e)=>{
       
        dispatch(changeFilterPriceMin(e.target.value))
    }
    const handChangeFilterPriceMax = (e)=>{
        
        dispatch(changeFilterPriceMax(e.target.value))
    }
    const handClickFilterBtn =()=>{
        dispatch(fetApiGetProduct(1, resDataFilter.itemPerPage, resConditionFilter))

        if(resConditionFilter.brand.length === 0 && resConditionFilter.color.length === 0
            && resConditionFilter.name === "" && resConditionFilter.price.min === "" && resConditionFilter.price.max === "" ){
                navigate("/products")
            }
    }
    return (
        <>
            <div className="container">
                
                <div className="brand-filter">
                    
                    <div className="tilte">
                        <h6>Brands</h6>
                    </div>
                    {resDataFilter.productBrandList.map((brand, index)=>{
                        return (
                            <div className="form-check" key={index}>
                                <input className="form-check-input" type="checkbox"  data-type="brand"  value={brand} id={brand} onChange={handcheckedFilter}></input>
                                <label className="form-check-lable" for={brand} >{brand}</label>
                            </div>
                        )
                    })}
                    
                </div>
                <div className="brand-filter mt-3">
                    <div className="tilte">
                        <h6 >Colors</h6>
                    </div>
                    {resDataFilter.productColorList.map((color, index)=>{
                        return (
                            <div className="form-check" key={index}>
                                <input className="form-check-input" type="checkbox" data-type = "color" value={color} id={color} onChange={handcheckedFilter}></input>
                                <label className="form-check-lable" for={color} >{color}</label>
                            </div>
                        )
                    })}
                    
                </div>
                <div className="brand-filter mt-3">
                    <div className="title">
                        <h6>Price</h6>
                    </div>
                    <div className="inp-price">
                        <input type="number" min={0} className="inp-min" value={resConditionFilter.price.min} onChange={handChangeFilterPriceMin} placeholder="min"/>
                        <span >-</span>
                        <input type="number" min={0} className="inp-min" value={resConditionFilter.price.max} onChange={handChangeFilterPriceMax} placeholder="max"/>
                    </div>

                </div>
                <div className="btn-filter mt-3">
                    <button className="btn btn-secondary w-50" onClick={handClickFilterBtn}>Filter</button>
                </div>
            </div>

        </>
    )
}
export default ProductFilter