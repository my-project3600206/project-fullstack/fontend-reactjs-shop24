import { useDispatch, useSelector } from "react-redux";
import "./shopCart.css"
import { Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import CancelIcon from '@mui/icons-material/Cancel';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import { changeInputVoucher, editAddQuanity, editMinusQuanity, fetchApiCheckVoucherCode, loadDataLocalStorage, removeCart, updateCart } from "../../actions/detail.Action";
import PaginationCart from "../paginationCart/paginationCart";
import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import { load } from "webfontloader";
const style={fontSize:"18px", color:"green", fontWeight:"bold"}
const style1={fontSize:"16px", color:"green", }
const style3={width:"50px", height:"36px"}
const style4 = {width: "80px", textAlign:"center", height:"36px"}
const style5 = {display:"flex", justifyContent:"center", alignItems:"center"}
const style6 = {width: "15%"}
function ShopCart(){
    const dispatch = useDispatch();
    const [numberPage, setNumberPage] = useState(1)
    const detailReducer = useSelector((reduxData)=>{
        return (reduxData.detailReducer)
    })
    const handRemoveCartClick = (idProduct)=>{
        dispatch(removeCart(idProduct))
    }
    const handBtnAddClick = (e)=>{
        console.log("e: ", e);
        dispatch(editAddQuanity(e))
    }
    const handBtnMinusClick = (e)=>{
        dispatch(editMinusQuanity(e))
    }
    const handQuanityOnchange = ()=>{

    }
    const handUpdateCartClick = ()=>{
        dispatch(updateCart())
    }
    const handChangeInputVoucherCode = (e)=>{
        dispatch(changeInputVoucher(e.target.value))
    }
    const handApplyVoucherClick = ()=>{
        dispatch(fetchApiCheckVoucherCode(detailReducer.voucherCode))
    }
    const productSelectedRender = detailReducer.productSelected.slice((detailReducer.currentPage - 1)*4, detailReducer.currentPage*4)
    useEffect(()=>{
        setNumberPage(Math.ceil(detailReducer.productSelected.length/4))
    }, [detailReducer.productSelected.length])
    useEffect(()=>{
        dispatch(loadDataLocalStorage())
    }, [])
    return(
        <>
        
        <Grid container mt={5}>
            <Grid item >
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table" >
                        <TableHead>
                            <TableRow >
                                <TableCell align="center" style={style}>Product</TableCell>
                                <TableCell align="center" style={style}>Image</TableCell>
                                <TableCell align="center" style={style}>Price</TableCell>
                                <TableCell align="center" style={style}>Quanity</TableCell>
                                <TableCell align="center" style={style}>SubTotal</TableCell>
                                <TableCell align="center" ></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {productSelectedRender.map((product, index)=>{
                                return (
                                    <TableRow hover key={index}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 },}} >
                                        <TableCell align="center" style={style1}>{product.name}</TableCell>
                                        <TableCell align="center" > <img src = {product.image} alt="image" style={style6}/></TableCell>
                                        <TableCell align="center" style={style1}>$ {product.promotionPrice}</TableCell>
                                        <TableCell align="center" style={style1}>
                                            <div style={style5}>
                                            <button className="form-control" style={style3} onClick={()=>handBtnMinusClick(product)}><RemoveIcon /></button>
                                            <input type="text" min={0} style={style4} value={product.quanity} className="form-control" onChange={handQuanityOnchange}/>
                                            <button className="form-control" style={style3} onClick={()=> handBtnAddClick(product)} ><AddIcon/></button>
                                            </div>
                                        
                                        </TableCell>
                                        <TableCell align="center" style={style1}>$ {Math.round(product.quanity * product.promotionPrice)}</TableCell>
                                        <TableCell align="center" >
                                            <CancelIcon onClick = {()=>handRemoveCartClick(product._id)} sx={{":hover": { borderRadius:"50%", color:"green"}, color:"gray"}}/>
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
    
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>

        </Grid>
        <Grid container>
            <Grid item>
                <PaginationCart numberPage = {numberPage}/>
            </Grid>
        </Grid>
        <Grid container mt={5}>
            <Grid item xs = {4} md = {4} lg = {4}>
                <div>
                    <Link to={"/products"}>
                    <button style={{width: "180px", height: "50px"}} className="btn-coupon">Continuous Shopping</button>
                    </Link>
                    
                </div>
                <div style={{marginTop: "20px"}}>
                    <h4>Discount code</h4>
                </div>
                <div style={{display:"flex"}}>
                    <input type="text" value={detailReducer.voucherCode} placeholder="voucher code here" className="form-control" style={{width:"200px"}} onChange={handChangeInputVoucherCode}/>
                    <button  className="btn-coupon" onClick={handApplyVoucherClick}>Apply coupon</button>
                </div>
                <div style={{marginTop: "10px"}}><p>sYiV5VBQ, 7XuILPnf, sCzVBCeo, QgqRNhgS</p></div>
                
            </Grid>
            <Grid item xs = {8} md = {8} lg = {8}>
                <Grid container>
                    <Grid item>
                        <button  className="btn-coupon" onClick={handUpdateCartClick} style={{width: "100px", height: "50px"}}>Update Cart</button>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs = {12} md = {12} lg = {12} style={{backgroundColor: "#FFDAB9", padding: "20px 0 0 20px"}}>
                        <Grid container>
                            <Grid item xs = {8} md = {8} lg = {8}>
                                <div className="table-total">
                                    <p>Total Quanity</p>
                                    
                                </div>
                            </Grid>
                            <Grid item xs = {4} md = {4} lg = {4}>
                                <div >
                                    <p>{detailReducer.totalQuanity}</p>
                                </div>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs = {8} md = {8} lg = {8}>
                                <div className="table-total">
                                    <p>Total Price</p>
                                  
                                </div>
                            </Grid>
                            <Grid item xs = {4} md = {4} lg = {4}>
                                <div >
                                    <p>{detailReducer.subTotalPrice} $</p>
                                </div>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs = {8} md = {8} lg = {8}>
                                <div className="table-total">
                                    <p>Discount Percent</p>
                                    
                                </div>
                            </Grid>
                            <Grid item xs = {4} md = {4} lg = {4}>
                                <div >
                                    <p>{detailReducer.voucherDiscount} %</p>
                                </div>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs = {8} md = {8} lg = {8}>
                                <div className="table-total">
                                    <p>Discount Price</p>
                                    
                                </div>
                            </Grid>
                            <Grid item xs = {4} md = {4} lg = {4}>
                                <div >
                                    <p>{Math.round(detailReducer.subTotalPrice*(detailReducer.voucherDiscount)/100)} $</p>
                                </div>
                            </Grid>
                        </Grid>
                        <hr style={{color: "black", size: "10px", marginTop: "-10px", width:"80%", }}/>
                        <Grid container>
                            <Grid item xs = {8} md = {8} lg = {8}>
                                <div className="table-total">
                                    <p>total payment</p>
                                    
                                </div>
                            </Grid>
                            <Grid item xs = {4} md = {4} lg = {4}>
                                <div >
                                    <p>{detailReducer.totalPrice} $</p>
                                </div>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs = {12} md = {12} lg = {12}>
                                <div >
                                 <button className="btn-checkout w-50">Go to checkout and payment</button>
                                </div>
                                
                            </Grid>

                        </Grid>
                        
                    </Grid>
                </Grid>
                

            </Grid>

        </Grid>
        </>
    )
}
export default ShopCart