import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import { Route, Routes } from "react-router-dom";
import HomePage from "./pages/homePages/homePage";
import ProductListPage from "./pages/productListPages/productListPage";
import HeaderComponent from "./components/headers/headerComponent";
import Footer from "./components/footer/footer";
import { useDispatch, useSelector } from "react-redux";
import ProductInfoPage from "./pages/productInfoPages/productInfoPage";
import { useEffect } from "react";
import { loadDataLocalStorage } from "./actions/detail.Action";
import ShopCartPage from "./pages/shopCartPages/shopCartPage";
import LoginPage from "./pages/loginPages/loginPage";
import SignUpPage from "./pages/signUpPages/signUpPage";



function App() {
  const resProduct = useSelector((reduxData) => {
    return (reduxData.productReducer)
  })
  const dispatch = useDispatch();
  useEffect(()=>{
    dispatch(loadDataLocalStorage())
  }, [])
  return (
    <>
      <HeaderComponent />
      <div className="App" style={{marginTop: "68px"}}>
        <Routes>
          <Route path="/" element={<HomePage />} />
          {/* <Route path="*" element={<HomePage />} /> */}
          <Route path="/products" element={<ProductListPage />} />
          <Route path="/productDetail/:productId" element={<ProductInfoPage />} />
          <Route path="/shopCart" element={<ShopCartPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/signUp" element={<SignUpPage />} />
        </Routes>
      </div>
      <Footer /> 

    </>

  );
}

export default App;
