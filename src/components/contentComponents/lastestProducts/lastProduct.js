import { useDispatch } from "react-redux";
import CardProduct from "../../cardProduct/cardProduct";
import "../content.Component.css";
import { useEffect } from "react";
import { fetApiGetProduct } from "../../../actions/product.Action";
import { useSelector } from "react-redux"
import { CircularProgress } from '@mui/material';
function LastProduct() {
    const dispatch = useDispatch();

    const respPoductInfo = useSelector((reduxData) => {
        return (reduxData.productReducer)
    })
    const conditionFilter = useSelector((reduxData) => {
        return (reduxData.filterReducer)
    })

    useEffect(() => {
        dispatch(fetApiGetProduct(1, 8, conditionFilter))
    }, [])
    return (
        <>
            <div className="container">
                <div className="last-product">
                    <div className="title-main" style={{display: (respPoductInfo.fetchProductPending || respPoductInfo.productList.length <=0) ? "none" : "block" }}>
                        <h4>LASTEST PRODUCTS</h4>
                    </div>
                    <div style={{ textAlign: "center",paddingTop:"150px", display: respPoductInfo.fetchProductPending ? "block" : "none" }}>
                        <CircularProgress />
                    </div>

                    <div className="card-product"  >
                        {respPoductInfo.productList.map((product, index) => (
                            
                            <CardProduct key={index} productId={product._id}
                                name={product.name}
                                imageUrl={product.imageUrl[0]}
                                description={product.description}
                                buyPrice={product.buyPrice}
                                promotionPrice={product.promotionPrice} />
                        ))}
                    </div>
                </div>
            </div>

        </>
    )
}
export default LastProduct