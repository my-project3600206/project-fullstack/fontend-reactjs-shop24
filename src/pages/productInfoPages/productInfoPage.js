import { useDispatch, useSelector } from "react-redux"
import BreadCrumb from "../../components/breadCrumb/breadCrumb"
import ProductDetail from "../../components/productDetail/productDetail"
import { useParams } from "react-router-dom";
import { fetApiGetProductById } from "../../actions/product.Action";
import { useEffect } from "react";

function ProductInfoPage(){
    const dispatch = useDispatch();
    const {productId} = useParams();
    console.log("productId ", productId)
   
    useEffect(()=>{
        window.scrollTo(0,0)
        dispatch(fetApiGetProductById(productId))
    },[productId])

    const resProductInfo = useSelector((reduxData)=>{
        return(reduxData.productReducer)
    })

    const breadCrumbList = [
        {
            url: "/",
            name: "Home"
        },
        {
            url: "/products",
            name: "All Product"
        },
        {
            name: resProductInfo.productById.name ? `${resProductInfo.productById.name}` : ""
        }

    ]
        
    return(
        <>
        <div>
            <BreadCrumb breadCrumbList = {breadCrumbList}/>
        </div>
        <div className="mt-5">
            <ProductDetail productId = {productId} productById = {resProductInfo.productById}/>
        </div>
        
        </>
    )
}
export default ProductInfoPage