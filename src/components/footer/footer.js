import { Link } from "react-router-dom";
import "../../components/footer/footer.css";
import FacebookIcon from '@mui/icons-material/Facebook';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TwitterIcon from '@mui/icons-material/Twitter';
import GoogleIcon from '@mui/icons-material/Google';
function Footer() {
    return (
        <>
            <div className="footer">
                <div className="col-sm-3">
                    <div className="footer-title">
                        <h6><b>PRODUCTS</b></h6>
                    </div>
                    <ul className="colum-item">
                        <li className="link-item">
                            <Link to="#">Help Center</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Contact Us</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Product Help</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Warrenty</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Oder Status</Link>
                        </li>
                    </ul>

                </div>
                <div className="col-sm-3">

                    <div className="footer-title">
                        <h6><b>SERVICE</b></h6>
                    </div>
                    <ul className="colum-item" >
                        <li className="link-item">
                            <Link to="#">Help Center</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Contact Us</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Product Help</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Warrenty</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Oder Status</Link>
                        </li>
                    </ul>


                </div>
                <div className="col-sm-3">

                    <div className="footer-title">
                        <h6><b>SUPPORT</b></h6>
                    </div>
                    <ul className="colum-item" >
                        <li className="link-item">
                            <Link to="#">Help Center</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Contact Us</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Product Help</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Warrenty</Link>
                        </li>
                        <li className="link-item">
                            <Link to="#">Oder Status</Link>
                        </li>
                    </ul>


                </div>
                <div className="col-sm-3">

                    <div className="footer-logo">
                        <img src="https://theme.hstatic.net/1000152881/1000926405/14/logo.png?v=642" alt="logo" height={60} />
                    </div>
                    <div className="footer-icon">
                        <div>
                            <GoogleIcon />
                        </div>
                        <div>
                            <FacebookIcon />
                        </div>
                        <div>
                            <YouTubeIcon />
                        </div>
                        <div>
                            <TwitterIcon />
                        </div>
                    </div>


                </div>
            </div>

        </>
    )
}
export default Footer