import { ADD_TO_CART, CHANGE_INPUT_VOUCHER, CHANGE_NUMBER_PAGE_CART, EDIT_ADD_QUANITY, EDIT_MINUS_QUANITY, FETCH_VOUCHER_ERROR, FETCH_VOUCHER_SUCCESS, LOAD_DATA_LOCAL_STORAGE, REMOVE_CART, UPDATE_CART } from "../constants/detail.Content";

const initialData = {
    totalQuanity: 0,
    subTotalQuanity: 0,
    totalPrice: 0,
    subTotalPrice: 0,
    productSelected: [],
    currentPage: 1,
    voucherCode: "",
    voucherDiscount: 0
   
}
const detailReducer = ((state = initialData, action) => {
   switch (action.type) {
    case ADD_TO_CART:
        const productSelected = localStorage.getItem("productSelected");
        const arrProductSelected = JSON.parse(productSelected)
        
        if(arrProductSelected){
            const checkProduct = arrProductSelected.filter((item)=>{
                return item._id === action.payload.productById._id
            })
            if(checkProduct.length > 0){
                arrProductSelected.forEach((product)=>{
                    if(product._id === action.payload.productById._id){
                        product.quanity =  product.quanity + action.payload.quanityProduct
                    }
                })
            }
            else{
                const newProduct = {
                    _id : action.payload.productById._id,
                    name: action.payload.productById.name,
                    quanity: action.payload.quanityProduct,
                    promotionPrice: action.payload.productById.promotionPrice,
                    image: action.payload.productById.imageUrl[0],
                }
                arrProductSelected.push(newProduct)
            }
            localStorage.setItem("productSelected", JSON.stringify(arrProductSelected));
            state.productSelected = arrProductSelected
            console.log("productSelected", productSelected)
           
        }
        else{
            const newProduct = {
                _id : action.payload.productById._id,
                name: action.payload.productById.name,
                quanity: action.payload.quanityProduct,
                promotionPrice: action.payload.productById.promotionPrice,
                image: action.payload.productById.imageUrl[0],
            }
            localStorage.setItem("productSelected", JSON.stringify([newProduct]));
            state.productSelected = [newProduct]
        }
        const calTotalQuanity = state.productSelected.reduce((acc, product)=>{
            return acc +=  product.quanity
        }, 0)
        state.totalQuanity =  calTotalQuanity
        state.subTotalQuanity = calTotalQuanity
        localStorage.totalQuanity = calTotalQuanity

        let totalPrice = state.productSelected.reduce((acc, product)=>{
            return acc += product.promotionPrice * product.quanity
        }, 0)
        state.subTotalPrice = Math.round(totalPrice)
        state.totalPrice = Math.round(totalPrice - (totalPrice * (state.voucherDiscount/100)))
        break;
    case LOAD_DATA_LOCAL_STORAGE:
        console.log("load data storage")
        const productCart = JSON.parse(localStorage.getItem("productSelected")) ;
        const toltalQuanity = localStorage.getItem("totalQuanity");
        console.log("toltalQuanity", toltalQuanity)
        console.log("productSelected", productCart)
        if(productCart){
            state.productSelected = productCart
            
            let totalPrice = state.productSelected.reduce((acc, product)=>{
                return acc += product.promotionPrice * product.quanity
            }, 0)
            state.subTotalPrice = Math.round(totalPrice)
            state.totalPrice = Math.round(totalPrice - (totalPrice * (state.voucherDiscount/100)))
        }
        if(toltalQuanity){
            state.totalQuanity =  toltalQuanity
            state.subTotalQuanity = toltalQuanity
        }

        break;
    case EDIT_ADD_QUANITY: {
        console.log("idProductget",action.payload.idProduct)
       
            state.productSelected.forEach((product)=>{
                if(product._id === action.payload.idProduct._id){
                    product.quanity +=1
                }
            })
           
            let totalPrice = state.productSelected.reduce((acc, product)=>{
                return acc += product.promotionPrice * product.quanity
            }, 0)
            state.subTotalPrice = Math.round(totalPrice)
            state.totalQuanity = state.productSelected.reduce((acc, product)=>{
                return acc += product.quanity
            }, 0)
            state.totalPrice = Math.round(totalPrice - (totalPrice * (state.voucherDiscount/100)))
    }
        break
    case EDIT_MINUS_QUANITY: {
        console.log("idProductget",action.payload.idProduct)
           
            state.productSelected.forEach((product)=>{
                if(product._id === action.payload.idProduct._id){
                    if(product.quanity > 0){
                        product.quanity -=1
                    }
                    
                }
            })
           
            let totalPrice = state.productSelected.reduce((acc, product)=>{
                return acc += product.promotionPrice * product.quanity
            }, 0)
            state.subTotalPrice = Math.round(totalPrice)
            state.totalQuanity = state.productSelected.reduce((acc, product)=>{
                return acc +=  product.quanity
            }, 0)
            state.totalPrice = Math.round(totalPrice - (totalPrice * (state.voucherDiscount/100)))
        }
            break
    case CHANGE_NUMBER_PAGE_CART:
        state.currentPage = action.payload.currentPage
        break
    case REMOVE_CART:{
        let newProductSelected = state.productSelected.filter((product)=>{
            return product._id !== action.payload.idProduct
        })
        state.productSelected = [...newProductSelected]
        let totalPrice = state.productSelected.reduce((acc, product)=>{
            return acc += product.promotionPrice * product.quanity
        }, 0)
        state.subTotalPrice = Math.round(totalPrice)
        state.totalQuanity = state.productSelected.reduce((acc, product)=>{
            return acc +=  product.quanity
        }, 0)
        state.totalPrice = Math.round(totalPrice - (totalPrice * (state.voucherDiscount/100)))
    }
        
        break
    case UPDATE_CART: {
        console.log("update cart")
        const calTotalQuanity = state.productSelected.reduce((acc, product)=>{
            return acc += product.quanity
        }, 0)
        const newCartProduct = state.productSelected.filter((product)=>{
            return product.quanity !== 0
        })
        localStorage.productSelected = JSON.stringify(newCartProduct)
        localStorage.totalQuanity = calTotalQuanity
        state.totalQuanity = calTotalQuanity
        state.productSelected = [...newCartProduct]
        state.subTotalQuanity = calTotalQuanity

    }
        break
    case CHANGE_INPUT_VOUCHER:
        state.voucherCode = action.payload.voucherCode
        console.log("state.voucherCode", state.voucherCode)
        break
    case FETCH_VOUCHER_SUCCESS: {
        state.voucherDiscount = action.payload.discount
        let totalPrice = state.productSelected.reduce((acc, product)=>{
            return acc += product.promotionPrice * product.quanity
        }, 0)
        state.totalPrice = Math.round(totalPrice - (totalPrice * (state.voucherDiscount/100)))
    }
        
    break
    case FETCH_VOUCHER_ERROR: {
        
        let totalPrice = state.productSelected.reduce((acc, product)=>{
            return acc += product.promotionPrice * product.quanity
        }, 0)
        state.totalPrice = Math.round(totalPrice)
    }
    break
    default:
        break;
   }

    return { ...state }
}) 
export default detailReducer
