import { useState, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import DetailCarousel from "./detailCarousel";
import "../productDetail/detailCarousel.css"
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { CircularProgress } from '@mui/material';
import CardProduct from "../cardProduct/cardProduct";
import ViewAllProduct from "../contentComponents/viewAll/viewAll";
import { addToCart } from "../../actions/detail.Action";
import { fetApiGetProduct } from "../../actions/product.Action";
import { useNavigate } from "react-router-dom";

const styleBtn = {width:"100px",height:"30px", textAlign:"center", fontWeight:"bold", fontSize:"20px"}
const styleP = {color: "green"}

function ProductDetail({productId, productById}){
    const [quanityProduct, setQuanityProduct] = useState(1);
    const [statusCheckStock, setCheckstock] = useState(false);
    const [imageChange, setImage] = useState(0);
    
    const dispatch = useDispatch();
    const navigate = useNavigate()
    const resPoductInfo = useSelector((reduxData)=>{
        return(reduxData.productReducer)
    })
    const filterReducer = useSelector((reduxData)=>{
        return(reduxData.filterReducer)
    })
    const detailReducer = useSelector((reduxData)=>{
        return(reduxData.detailReducer)
    })
    const handClickAddIcon = (e)=>{
        console.log("add clicked")
       setQuanityProduct(quanity =>{
            if(quanity >= resPoductInfo.productById.amount){
                setCheckstock(true)
                return quanity
            }else{
                return quanity + 1
            }
       })
       
    }
    const handClickMinusIcon = (e)=>{
        setQuanityProduct(quanity =>{
            if(quanity <= 1){
                return 1
            }else{
                setCheckstock(false)
                return quanity - 1
            }
       })
    }
    const handchangeQuanity = (e)=>{
        setQuanityProduct(e.target.value);
        if(quanityProduct > resPoductInfo.productById.amount){
            setCheckstock(true)
        }else{
            setCheckstock(false)
        }
    }
    const handClickAddToCart = (e)=>{
        dispatch(addToCart(quanityProduct, productById))
    }
    const handClickBuy = (e)=>{
        navigate("/shopCart")
    }
    useEffect(() => {
        setQuanityProduct(1)
    }, [productId])

    useEffect(() => {
        dispatch(fetApiGetProduct(1, resPoductInfo.itemPerPage, filterReducer))
    }, [])
    return(
        <>
        <div className="container">
            {resPoductInfo ? 
            (<div className="detail-wrapper">
            <div className="row">
                <div className="col-5">
                    <div className="detail-img">
                        <img src={resPoductInfo?.productById?.imageUrl?.[imageChange]} alt="" style={{width: "100%"}}/>
                    </div>
                    <div className="detail-carousel">
                        <DetailCarousel resPoductInfo = {resPoductInfo} setImage = {setImage} />
                    </div>

                </div>
                <div className="col-7">
                    <div className="detail-colum">
                        <div className="detail-title">
                            <h4>{resPoductInfo.productById.name}</h4>
                        </div>
                        <div className="detail-brand">
                            <p className="brand"> Brand: </p> 
                            <p style={styleP}>{resPoductInfo.productById.brand}</p>
                            <p className="brand" style={{marginLeft: "50px"}}> Status: </p> 
                            {resPoductInfo.productById.amount > 0 ? <p style={styleP}>Stocking</p> : <p style={styleP}>Not Stock</p>}
                        </div>
                        <div className="detail-color">
                            <p>Color :  {resPoductInfo.productById.color}</p>
                        </div>
                        <div className="detail-content">
                            <p>{resPoductInfo.productById.name} {resPoductInfo.productById.description}</p>
                        </div>
                        <div className="detail-price">
                            <p>$ {resPoductInfo.productById.promotionPrice}</p>
                        </div>
                        <div style={{color: "red"}}>
                            {statusCheckStock ? `Stock is not enough, please choose maximum  ${resPoductInfo.productById.amount} items`:null}
                        </div>
                        <div className="form-group detail-button">
                           <div className="btn-add" onClick={handClickMinusIcon}>
                                <RemoveCircleIcon/>
                            </div>
                            
                            <div className="inp-quantity">
                                <input type="text" className="form-control" value={quanityProduct} style={styleBtn} onChange={handchangeQuanity} />
                            </div>
                            
                            <div className="btn-add" onClick={handClickAddIcon}>
                                <AddCircleIcon />
                            </div>
                            
                        </div>
                        <div className="addToCart mt-4">
                            <button className="btn btn-info add-cart w-50" onClick={handClickAddToCart}>ADD TO CART</button>
                        </div>
                        <div className="btn-buy mt-4">
                            <button className="btn btn-warning buy-here w-50" onClick={handClickBuy} >CLICK BUY HERE</button>
                        </div>
                    </div>
                    
                </div>

            </div>
            <div className="detail-description">
                <h5>Description</h5>
                <p>Tai nghe kiểm âm Audio Technica ATH-M20x cơ bản chuyên dùng cho các Studio Tracking và Mixing. Hàng chính hãng, mẫu hộp mới. - Chất lượng và kỹ thuật xây dựng tiên tiến. - Trình điều khiển 40 mm với nam châm đất hiếm và cuộn dây thoại bằng nhôm bọc đồng - Điều chỉnh để nâng cao hiệu suất tần số thấp - Các đường viền thiết kế hình tròn xung quanh tai giúp cách ly âm thanh tuyệt vời trong môi trường ồn ào - Lối ra cáp một bên thuận tiện - Được thiết kế để sử dụng Tracking và Mixing</p>

            </div>
            <div className="row ">
                <div className="raleted-wrapper">
                    <div className="related-title">
                        <h5>Related Products</h5>
                    </div>
                </div>

                <div className="related-product"  >
                    {resPoductInfo.productList.slice(0,4).map((product, index) => (
                        
                        <CardProduct key={index} productId={product._id}
                            name={product.name}
                            imageUrl={product.imageUrl[0]}
                            description={product.description}
                            buyPrice={product.buyPrice}
                            promotionPrice={product.promotionPrice} />
                    ))}
                </div>

            </div>
            <div className="btn-vieuAll">
                <ViewAllProduct/>
            </div>

            </div>) : <CircularProgress />
            }
            

        </div>
        </>
    )
}
export default ProductDetail